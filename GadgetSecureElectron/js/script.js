// Mixing jQuery and Node.js code in the same file? Yes please!
$(function() {
    // window onload fucntion


    // MAC address detection
    var macaddress = require('node-macaddress');
    macaddress.one(function(err, mac) {
        console.log("Mac address for awdl0: %s", mac);
        $('#imei').val(mac);
        $('#reimei').val(mac);
    });

      display_ct()
      

      // getSerialNumber

      const exec = require('child_process').exec;

        exec('sudo -S dmidecode -s system-serial-number', (e, stdout, stderr)=> {

            if (e instanceof Error) {

                console.error(e);

                throw e;

            }

            console.log('stdout ', stdout);

            console.log('stderr ', stderr);

        });


});


      function display_c(){
        var refresh=1000; // Refresh rate in milli seconds
        mytime=setTimeout('display_ct()',refresh)
      }

      function display_ct() {
        var strcount
        var x = new Date()
        document.getElementById('ct').innerHTML = x;
        tt=display_c();
      }


function setenddate(){
    $("#endDate").attr("min",$("#startDate").val());
}

function setplan(){    
        if (0 <= $('#invoiceValue').val() && $('#invoiceValue').val() <=4000) {
                $('#plan').val("safea").attr('selected','selected');
        }else if (4001 <= $('#invoiceValue').val() && $('#invoiceValue').val() <=6000){
            $('#plan').val("safeb").attr('selected','selected');
        }else if (6001 <= $('#invoiceValue').val() && $('#invoiceValue').val() <=15000){
            $('#plan').val("safec").attr('selected','selected');
        } else{
            $('#plan').val("safed").attr('selected','selected');
        }
    
}

function checkLogin() {

    $('#preloader').css('display','block');

    
    if ($('#username').val() != '' && $('#password').val() != '') {
        debugger

        // testing
        // $(location).attr('href', 'file://' + __dirname + '/html/admin/admin-home.html');

        $.ajax({
            url: "http://localhost:8080/LoginCheck/checkLogin?username=" + $('#username').val() + "&password=" + $('#password').val() + "",
            cache: false,
            success: function(res) {
                console.log(res);

                debugger;
                // Administrator
                if (res.id == '1') {
                    console.log('Administrator');

                    localStorage.setItem("role", "Administrator");
                    localStorage.setItem("username", $('#username').val());
                    $(location).attr('href', 'file://' + __dirname + '/html/admin/admin-home.html');
                }
                // Company Sales Head
                else if(res.id == '8'){
                    console.log('CompanySalesHead');

                    localStorage.setItem("role", "CompanySalesHead");
                    localStorage.setItem("username", $('#username').val());
                    $(location).attr('href', 'file://' + __dirname + '/html/CompanySalesHead/companysaleshead-home.html');
                }
                // Sales Team                
                else if (res.id == '7') {
                    console.log('SalesTeam');

                    localStorage.setItem("role", "SalesTeam");
                    localStorage.setItem("username", $('#username').val());
                    $(location).attr('href', 'file://' + __dirname + '/html/SalesTeam/sales-home.html');
                }
                // Regional Sales Manager                
                else if (res.id == '9') {
                    console.log('RegionalSalesManager');

                    localStorage.setItem("role", "RegionalSalesManager");
                    localStorage.setItem("username", $('#username').val());
                    $(location).attr('href', 'file://' + __dirname + '/html/RegionalSalesManager/rsm-home.html');
                }
                 // Distributor                
                else if (res.id == '2') {
                    console.log('Distributor');

                    localStorage.setItem("role", "Distributor");
                    localStorage.setItem("username", $('#username').val());
                    $(location).attr('href', 'file://' + __dirname + '/html/Distributor/distributor-home.html');
                }
                // Dealer                
                else if (res.id == '4') {
                    console.log('Dealer');

                    localStorage.setItem("role", "Dealer");
                    localStorage.setItem("username", $('#username').val());
                    $(location).attr('href', 'file://' + __dirname + '/html/Dealer/dealer-home.html');
                }
                 else {
                    console.log("Failed");
                    alert("**Wrong username/password");
                }
                $('#preloader').css('display','none');

            }
        });

    } else {
        $('#preloader').css('display','none');
        alert('*** Please enter valid username and password');
    }
}


function validemail() {
    filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test($('#email').val())) {
        $('#valemail').html("");
        return true;
    } else {
        $('#valemail').html("* Please enter valid email id");
        $('#email').focus();
        return false;
    }
}


function validpan() {
    filter = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
    if (filter.test($('#pan').val())) {
        $('#valpan').html("");
        return true;
    } else {
        $('#valpan').html("* Please enter valid Pan No.");
       /* $('#pan').focus();*/
        return false;
    }
}

function validgstn() {
    filter = /^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9]{1}[zZ]{1}[0-9]{1})+$/;
    if (filter.test($('#gstn').val())) {
        $('#valgstn').html("");
        return true;
    } else {
        $('#valgstn').html("* Please enter valid GSTN No.");
        $('#gstn').focus();
        return false;
    }
}

 function chkaadhar() {
    if($('#aadhar').val().length < 12) {
        $('#er1').remove();
        $('#divaadhar').append("<h6 id='er1' style='color: red;'>* Aadhar Number must be of 12 digit.</h6>");
        $('#aadhar').focus();
        return false;
} else{
        $('#er1').remove();
        return true;
        }
}


function chkmobile(){
    if($('#mobile').val().length < 10){
        $('#chkmobileh6').remove();
        $('#divmobile').append("<h6 style='color:red' id='chkmobileh6'> * Mobile Number must be of 10 digit.</span>");
        $('#mobile').focus();
        return false;
    }else if($('#mobile').val() === $('#altmobile').val()){
        $('#chkmobileh6').remove();
        $('#divmobile').append("<h6 style='color:red' id='chkmobileh6'> * Alternate Mobile No. and mobile no. must be different</span>");
        $('#mobile').focus();
    } else{
        $('#chkmobileh6').remove();
        return true;
    }
}

function atmobile() {
    if ($('#mobile').val() === $('#altmobile').val()) {
        $('#attmobile').remove();
        $('#divaltmobile').append("<h6 id='attmobile' style='color: red;'>* Alternate Mobile No. and mobile no. must be different</h6>");
    } else if ($('#altmobile').val().length <10) {
        $('#attmobile').remove();
        $('#divaltmobile').append("<h6 id='attmobile' style='color: red;'> * Alternate Mobile Number Must be of 10 digit </h6>");
    }else{
        $('#attmobile').remove();
        }
}




function atpassword() {
    if ($('#password').val() === $('#confirmpassword').val()) {
        $('#pwd-error').remove();
        return true;
    } else{
        $('#pwd-error').remove();
        $('#crnfrmpwd').append("<h6 id='pwd-error' style='color: red;'>* Password and confirm password must be same</h6>");
       /* $('#confirmpassword').focus();*/
        return false;
        }
}






function registerGadget() {

    var plan = $('#plan').val();
    var gender = $('#gender').val();
    var state = $('#state').val();
    var pincode = $('#pincode').val();
    var mobile = $('#mobile').val();
    var email = $('#email').val();
    var purchaseType = $('#purchaseType').val();
    var invoiceNumber = $('#invoiceNumber').val();
    var deviceType = $('#deviceType').val();

    var brand = $('#brand').val();
    var imei = $('#imei').val();
    var customername = $('#customername').val();
    var address = $('#address').val();
    var city = $('#city').val();
    var dob = $('#dob').val();
    var altmobile = $('#altmobile').val();
    var reemail = $('#reemail').val();
    var purchaseFrom = $('#purchaseFrom').val();

    var invoiceValue = $('#invoiceValue').val();
    var dop = $('#dop').val();
    var model = $('#model').val();
    var reimei = $('#reimei').val();

    var username = localStorage.getItem('username');
    
    debugger;

    if ($('#plan').val() != '' && $('#gender').val() != '' && $('#state').val() != '' && $('#pincode').val() != '' && $('#mobile').val() != '' && $('#email').val() != '' && $('#purchaseType').val() != '' && $('#invoiceNumber').val() != '' && $('#deviceType').val() != '' && $('#brand').val() != '' && $('#imei').val() != '' && $('#customername').val() != '' && $('#address').val() != '' && $('#city').val() != '' && $('#dob').val() != '' && $('#reemail').val() != '' && $('#purchaseFrom').val() != '' && $('#invoiceValue').val() != '' && $('#dop').val() != '' && $('#model').val() != '') {

        $("#loader").css("display", "block");
        $("#submitButton").attr('disabled', 'true');


        var http = require('http');
        var fs = require('fs');

        var file = fs.FileWriteStream("Invoice"+email+".pdf");
        var request = http.get("http://localhost:8080/Gadget/saveGadget?plan=" + plan + "&gender=" + gender + "&state=" + state + "&pincode=" + pincode + "&mobile=" + mobile + "&email=" + email + "&purchaseType=" + purchaseType + "&purchaseFrom=" + purchaseFrom + "&invoiceNumber=" + invoiceNumber + "&deviceType=" + deviceType + "&brand=" + brand + "&imei=" + imei + "&customername=" + customername + "&address=" + address + "&city=" + city + "&dob=" + dob + "&altmobile=" + altmobile + "&invoiceValue=" + invoiceValue + "&dop=" + dop + "&model=" + model + "&salesteam="  + localStorage.getItem("username") + "&submittedBy="+username+"", function(response) {
          response.pipe(file);
          $('#download').append('<a id="downloadInvoice" href="../../Invoice'+email+'.pdf" style="color: green;" download><i class="fa fa-file"></i>   SAVE RECEIPT</a>');          
           $("#loader").css("display", "none");
        });
        // $('#gadgetreg')[0].reset();



    } else {
        $('#err').empty();
        $('#err').append("<h6>Note: No changes will be accepted in the customer's profile once the coupon is registered by the customer.</h6><h6>*All fields are Mandatory.</h6>");

        alert("*** All fields are Mandatory...");
    }



}


// load admin role assign dropdowns

function getList(){

        $.ajax({
            url: "http://localhost:8080/Admin/getDistributor",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#exampleSelect1').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect1').append('<option value="new">CREATE NEW</option>');

            }
        });

        $.ajax({
            url: "http://localhost:8080/Admin/getSubDistributor",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#exampleSelect2').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect2').append('<option value="new">CREATE NEW</option>');

            }
        });

        $.ajax({
            url: "http://localhost:8080/Admin/getDealer",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#exampleSelect3').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect3').append('<option value="new">CREATE NEW</option>');

            }
        });
        $.ajax({
            url: "http://localhost:8080/Admin/getRetailer",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#exampleSelect4').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect4').append('<option value="new">CREATE NEW</option>');

            }
        });

        $.ajax({
            url: "http://localhost:8080/Admin/getSalesHead",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#exampleSelect5').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect5').append('<option value="new">CREATE NEW</option>');

            }
        });

        $.ajax({
            url: "http://localhost:8080/Admin/getSalesTeam",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#exampleSelect6').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect6').append('<option invoice="new">CREATE NEW</option>');

            }
        });


}



function submitUserCreationForm(){
    var fname = $('#fname').val();
    var lname = $('#lname').val();
    var address = $('#address').val();
    var email = $('#email').val();
    var mobile = $('#mobile').val();
    var userType = $('#userType').val();

    var password = $('#password').val();
    var confirmpassword = $('#confirmpassword').val();
    var city = $('#city').val();

    var state = $('#state').val();


    var pincode = $('#pincode').val();

    var firmname = $('#firmname').val();
    var gstn = $('#gstn').val();
    var aadhar = $('#aadhar').val();
    var pan = $('#pan').val();
    
    if( fname != '' && lname != '' && address != '' && email != '' && mobile != '' && userType != '' && password != '' && confirmpassword != '' && city != '' && state != '' && pincode != '' && firmname != '' && gstn != '' && aadhar != '' && pan != ''){
        $('#error').html('');
    $.ajax({
            url: "http://localhost:8080/Admin/saveUser?fname="+$('#fname').val()+"&lname="+$('#lname').val()+"&address="+$('#address').val()+"&email="+$('#email').val()+"&userType="+$('#userType').val()+"&mobile="+$('#mobile').val()+"&password="+$('#password').val()+"&city="+$('#city').val()+"&state="+$('#state').val()+"&pincode="+$('#pincode').val()+"&altmobile="+$('#altmobile').val()+"&firmname="+firmname+"&gstn="+gstn+"&aadhar="+aadhar+"&pan="+pan,
            cache: false,
            success: function(res) {

                if (res == 'success') {
                    $('#userForm')[0].reset();
                    $('#response').html('Successfully saved...');
                }
                else{
                    $('#response').html('Please try again...');
                }

            }
        });
}else{
    $('#error').html('*All fields are Mandatory.');
    alert('*All fields are Mandatory.');
    return false;
}
}


function distributor(){
    
    console.log('distributor change');

    $('#exampleSelect2').attr('disabled','true');
    $('#exampleSelect3').attr('disabled','true');
    $('#exampleSelect4').attr('disabled','true');
    $('#exampleSelect5').attr('disabled','true');
    $('#exampleSelect6').attr('disabled','true');

    $('#dealer').css('display', 'none');
    $('#retailer').css('display', 'none');
    $('#saleshead').css('display', 'none');
    $('#salesteam').css('display', 'none');

   
    
    $.ajax({
            url: "http://localhost:8080/Admin/getSubDistributor",
            cache: false,
            success: function(res) {
             console.log(res);
             $('#selectedSubdistributor').html('');
              $.each(res, function(i, item) {  
             $('#selectedSubdistributor').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
             })
             

            }
        });
}
function subdistributor(){
    console.log('subdistributor change');

    $('#exampleSelect1').attr('disabled','true');
    $('#exampleSelect3').attr('disabled','true');
    $('#exampleSelect4').attr('disabled','true');
    $('#exampleSelect5').attr('disabled','true');
    $('#exampleSelect6').attr('disabled','true');

       $('#subdistributor').css('display', 'none');
    $('#retailer').css('display', 'none');
    $('#saleshead').css('display', 'none');
    $('#salesteam').css('display', 'none');


    
    $.ajax({
            url: "http://localhost:8080/Admin/getDealer",
            cache: false,
            success: function(res) {
             console.log(res);
             $('#selectDealers').html('');
              $.each(res, function(i, item) {  
             $('#selectDealers').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
             })
             

            }
        });
}
function dealer(){
    console.log('dealer change');

    $('#exampleSelect1').attr('disabled','true');
    $('#exampleSelect2').attr('disabled','true');
    $('#exampleSelect4').attr('disabled','true');
    $('#exampleSelect5').attr('disabled','true');
    $('#exampleSelect6').attr('disabled','true');

    $('#subdistributor').css('display', 'none');
    $('#dealer').css('display', 'none');
    
    $('#saleshead').css('display', 'none');
    $('#salesteam').css('display', 'none');

    
    
    $.ajax({
            url: "http://localhost:8080/Admin/getRetailer",
            cache: false,
            success: function(res) {
             console.log(res);
             $('#selectRetailers').html('');
              $.each(res, function(i, item) {  
             $('#selectRetailers').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
             })
             

            }
        });
}

function retailer(){
    console.log('retailer change');

        console.log('dealer change');

    $('#exampleSelect1').attr('disabled','true');
    $('#exampleSelect2').attr('disabled','true');
    $('#exampleSelect3').attr('disabled','true');
    $('#exampleSelect5').attr('disabled','true');
    $('#exampleSelect6').attr('disabled','true');

    $('#subdistributor').css('display', 'none');
    $('#dealer').css('display', 'none');
    $('#retailer').css('display', 'none');
    
    $('#salesteam').css('display', 'none');
    
    
    $.ajax({
            url: "http://localhost:8080/Admin/getSalesHead",
            cache: false,
            success: function(res) {
             console.log(res);
             $('#selectSalesHead').html('');
              $.each(res, function(i, item) {  
             $('#selectSalesHead').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
             })
             

            }
        });
}
function saleshead(){
    console.log('saleshead change');
    $('#exampleSelect1').attr('disabled','true');
    $('#exampleSelect2').attr('disabled','true');
    $('#exampleSelect3').attr('disabled','true');
    $('#exampleSelect4').attr('disabled','true');
    $('#exampleSelect6').attr('disabled','true');

    $('#subdistributor').css('display', 'none');
    $('#dealer').css('display', 'none');
    $('#retailer').css('display', 'none');
    $('#saleshead').css('display', 'none');
    // $('#salesteam').css('display', 'none');
        $.ajax({
            url: "http://localhost:8080/Admin/getSalesTeam",
            cache: false,
            success: function(res) {
             console.log(res);
             $('#selectSalesTeam').html('');
              $.each(res, function(i, item) {  
             $('#selectSalesTeam').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
             })
             

            }
        });
}



// update users hirarchy

function saveSubDistributor(){
    console.log('saveSubDistributor');
    $.ajax({
            url: "http://localhost:8080/Admin/updateSubdistributor?distributor="+$('#exampleSelect1').val()+"&subdistributors="+$('#selectedSubdistributor').val()+"",
            cache: false,
            success: function(res) {
             console.log(res);
             if (res == 'success') {
                alert('Data updated...');
                $('#submitDealers').attr('disabled', 'true');
             }
             else{
                alert('Please try again...');  
             }
             
            }
        });

}


function saveDealers(){
    console.log('saveDealers');
    $.ajax({
            url: "http://localhost:8080/Admin/updateDelaer?subdistributor="+$('#exampleSelect2').val()+"&dealers="+$('#selectDealers').val()+"",
            cache: false,
            success: function(res) {
             console.log(res);
             if (res == 'success') {
                alert('Data updated...');
                $('#submitDealers').attr('disabled', 'true');
             }
             else{
                alert('Please try again...');  
             }
             
            }
        });

}


function saveRetailers(){
    console.log('saveRetailers');
    $.ajax({
            url: "http://localhost:8080/Admin/updateRetailer?dealer="+$('#exampleSelect3').val()+"&retailers="+$('#selectRetailers').val()+"",
            cache: false,
            success: function(res) {
             console.log(res);
             if (res == 'success') {
                alert('Data updated...');
                $('#submitRetailers').attr('disabled', 'true');
             }
             else{
                alert('Please try again...');  
             }
             
            }
        });

}



function saveSalesHead(){
    console.log('saveSalesHead');
    $.ajax({
            url: "http://localhost:8080/Admin/updateSalesHead?retailer="+$('#exampleSelect4').val()+"&salesheads="+$('#selectSalesHead').val()+"",
            cache: false,
            success: function(res) {
             console.log(res);
             if (res == 'success') {
                alert('Data updated...');
                $('#submitSalesHead').attr('disabled', 'true');
             }
             else{
                alert('Please try again...');  
             }
             
            }
        });

}


function saveSalesTeam(){
    console.log('saveSalesTeam');
    $.ajax({
            url: "http://localhost:8080/Admin/updateSalesTeam?saleshead="+$('#exampleSelect5').val()+"&salesteam="+$('#selectSalesTeam').val()+"",
            cache: false,
            success: function(res) {
             console.log(res);
             if (res == 'success') {
                alert('Data updated...');
                $('#submitSalesTeam').attr('disabled', 'true');
             }
             else{
                alert('Please try again...');  
             }
             
            }
        });

}


// target tracking page calls

function trackTargetRoleSelector(){

    $('#type').html($('#roleSelector').val());
    
    if ($('#roleSelector').val() == 'distributor') {
        console.log($('#roleSelector').val());

        // $('#distributor').attr('disabled', 'true');
        $('#subdistributor').attr('disabled', 'true');
        $('#dealer').attr('disabled', 'true');
        $('#retailer').attr('disabled', 'true');
        $('#saleshead').attr('disabled', 'true');
        $('#salesteam').attr('disabled', 'true');

        $.ajax({
            url: "http://localhost:8080/Admin/getDistributor",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#distributor').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect1').append('<option value="new">CREATE NEW</option>');

            }
        });
    }

    if ($('#roleSelector').val() == 'subdistributor') {
     console.log($('#roleSelector').val());
             $('#distributor').attr('disabled', 'true');
        // $('#subdistributor').attr('disabled', 'true');
        $('#dealer').attr('disabled', 'true');
        $('#retailer').attr('disabled', 'true');
        $('#saleshead').attr('disabled', 'true');
        $('#salesteam').attr('disabled', 'true');
     $.ajax({
            url: "http://localhost:8080/Admin/getSubDistributor",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#subdistributor').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect1').append('<option value="new">CREATE NEW</option>');

            }
        });
    }

    if ($('#roleSelector').val() == 'dealer') {
        console.log($('#roleSelector').val());
                $('#distributor').attr('disabled', 'true');
        $('#subdistributor').attr('disabled', 'true');
        // $('#dealer').attr('disabled', 'true');
        $('#retailer').attr('disabled', 'true');
        $('#saleshead').attr('disabled', 'true');
        $('#salesteam').attr('disabled', 'true');
        $.ajax({
            url: "http://localhost:8080/Admin/getDealer",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#dealer').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect1').append('<option value="new">CREATE NEW</option>');

            }
        });
    }

    if ($('#roleSelector').val() == 'retailer') {
        console.log($('#roleSelector').val());
                $('#distributor').attr('disabled', 'true');
        $('#subdistributor').attr('disabled', 'true');
        $('#dealer').attr('disabled', 'true');
        // $('#retailer').attr('disabled', 'true');
        $('#saleshead').attr('disabled', 'true');
        $('#salesteam').attr('disabled', 'true');
        $.ajax({
            url: "http://localhost:8080/Admin/getRetailer",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#retailer').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect1').append('<option value="new">CREATE NEW</option>');

            }
        });
    }

    if ($('#roleSelector').val() == 'saleshead') {
        console.log($('#roleSelector').val());
                $('#distributor').attr('disabled', 'true');
        $('#subdistributor').attr('disabled', 'true');
        $('#dealer').attr('disabled', 'true');
        $('#retailer').attr('disabled', 'true');
        // $('#saleshead').attr('disabled', 'true');
        $('#salesteam').attr('disabled', 'true');
        $.ajax({
            url: "http://localhost:8080/Admin/getSalesHead",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#saleshead').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect1').append('<option value="new">CREATE NEW</option>');

            }
        });
    }

    if ($('#roleSelector').val() == 'salesteam') {
        console.log($('#roleSelector').val());
                $('#distributor').attr('disabled', 'true');
        $('#subdistributor').attr('disabled', 'true');
        $('#dealer').attr('disabled', 'true');
        $('#retailer').attr('disabled', 'true');
        $('#saleshead').attr('disabled', 'true');
        // $('#salesteam').attr('disabled', 'true');
        $.ajax({
            url: "http://localhost:8080/Admin/getSalesTeam",
            cache: false,
            success: function(res) {
                $.each(res, function(i, item) {                    
                    $('#salesteam').append('<option value="'+res[i].id+'">'+res[i].email+'</option>');
                })
                // $('#exampleSelect1').append('<option value="new">CREATE NEW</option>');

            }
        });
    }
}

// save reward 
function saveReward(){
    console.log('saveReward');
            $.ajax({
            url: "http://localhost:8080/Admin/saveRewardDetails?startDate="+$('#startDate').val()+"&endDate="+$('#endDate').val()+"&rewardType="+$('#rewardType').val()+"&rewardDetails="+$('#rewardDetails').val()+"",
            cache: false,
            success: function(res) {
              console.log(res);
              if (res == 'success') {
                $('#status').css('display','block');
              }
              else{
                $('#status').css('display','block');
                $('#status').html('');
                $('#status').html('Please try again...');
              }
            }
        });

}


// load set targets data

function saveTarget(){

    debugger;
    
    if ($('#type').html() == 'distributor') {
        $.ajax({
            url: "http://localhost:8080/Admin/saveTarget?startDate="+$('#startDate').val()+"&endDate="+$('#endDate').val()+"&totalTarget="+$('#totalTarget').val()+"&userId="+$('#distributor').val()+"",
            cache: false,
            success: function(res) {
              console.log(res);
              if (res == 'success') {
                $('#status').css('display','block');
              }
              else{
                $('#status').css('display','block');
                $('#status').html('');
                $('#status').html('Please try again...');
              }
            }
        });
    }

    if ($('#type').html() == 'subdistributor') {
        $.ajax({
            url: "http://localhost:8080/Admin/saveTarget?startDate="+$('#startDate').val()+"&endDate="+$('#endDate').val()+"&totalTarget="+$('#totalTarget').val()+"&userId="+$('#subdistributor').val()+"",
            cache: false,
            success: function(res) {
              console.log(res);
              if (res == 'success') {
                $('#status').css('display','block');
              }
              else{
                $('#status').css('display','block');
                $('#status').html('');
                $('#status').html('Please try again...');
              }
            }
        });
    }

    if ($('#type').html() == 'dealer') {
        $.ajax({
            url: "http://localhost:8080/Admin/saveTarget?startDate="+$('#startDate').val()+"&endDate="+$('#endDate').val()+"&totalTarget="+$('#totalTarget').val()+"&userId="+$('#dealer').val()+"",
            cache: false,
            success: function(res) {
              console.log(res);
              if (res == 'success') {
                $('#status').css('display','block');
              }
              else{
                $('#status').css('display','block');
                $('#status').html('');
                $('#status').html('Please try again...');
              }
            }
        });
    }

    if ($('#type').html() == 'retailer') {
        $.ajax({
            url: "http://localhost:8080/Admin/saveTarget?startDate="+$('#startDate').val()+"&endDate="+$('#endDate').val()+"&totalTarget="+$('#totalTarget').val()+"&userId="+$('#retailer').val()+"",
            cache: false,
            success: function(res) {
              console.log(res);
              if (res == 'success') {
                $('#status').css('display','block');
              }
              else{
                $('#status').css('display','block');
                $('#status').html('');
                $('#status').html('Please try again...');
              }
            }
        });
    }

    if ($('#type').html() == 'saleshead') {
        $.ajax({
            url: "http://localhost:8080/Admin/saveTarget?startDate="+$('#startDate').val()+"&endDate="+$('#endDate').val()+"&totalTarget="+$('#totalTarget').val()+"&userId="+$('#saleshead').val()+"",
            cache: false,
            success: function(res) {
              console.log(res);
              if (res == 'success') {
                $('#status').css('display','block');
              }
              else{
                $('#status').css('display','block');
                $('#status').html('');
                $('#status').html('Please try again...');
              }
            }
        });
    }

    if ($('#type').html() == 'salesteam') {
        $.ajax({
            url: "http://localhost:8080/Admin/saveTarget?startDate="+$('#startDate').val()+"&endDate="+$('#endDate').val()+"&totalTarget="+$('#totalTarget').val()+"&userId="+$('#salesteam').val()+"",
            cache: false,
            success: function(res) {
              console.log(res);
              if (res == 'success') {
                $('#status').css('display','block');
              }
              else{
                $('#status').css('display','block');
                $('#status').html('');
                $('#status').html('Please try again...');
              }
            }
        });
    }

}